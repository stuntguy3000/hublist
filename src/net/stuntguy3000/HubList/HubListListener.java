package net.stuntguy3000.HubList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class HubListListener implements Listener {
	
	private HubListPlugin plugin;
	
	public HubListListener (HubListPlugin instance)
	{
		this.plugin = instance;
	}
	
	@EventHandler
	public void PlayerJoinEvent (PlayerJoinEvent event)
	{
		Player p = event.getPlayer();
		
		plugin.HubListDatabase.sendQuery("");
	}

	@EventHandler
	public void PlayerQuitEvent (PlayerQuitEvent event)
	{
		Player p = event.getPlayer();
		
		plugin.HubListDatabase.sendQuery("");
	}

	@EventHandler
	public void PlayerKickEvent (PlayerKickEvent event)
	{
		Player p = event.getPlayer();
		
		plugin.HubListDatabase.sendQuery("");
	}
	
}
