package net.stuntguy3000.HubList;

import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class HubListPlugin extends JavaPlugin implements Listener{
	
	Logger log = Bukkit.getLogger();
	
	public HubListDatabase HubListDatabase;
	
	public void onEnable() {
		try {
		    Metrics metrics = new Metrics(this);
		    metrics.start();
		} catch (IOException e) {
			log.severe("[HubList] Unable to connect to metrics!");
		    e.printStackTrace();
		}
		
		HubListDatabase = new HubListDatabase(this);
		HubListDatabase.sendQuery("Try to create DB");
	}
	
	public void onDisable() {
		HubListDatabase.sendQuery("Remove all users");
	}
	 
	public String c(String input) {
		return ChatColor.translateAlternateColorCodes('&', input);
	}
}
